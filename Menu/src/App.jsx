/* eslint-disable no-unused-vars */
import { useState } from "react"
import data from './data'
import Menu from "./Menu"
import Categories from "./Categories"

const allCategories = ['ALL', ...new Set(data.map((item) => item.category))]
console.log(allCategories)
function App() {
  const [menu, setMenu] = useState(data);
  const [categories, setCategories] = useState(allCategories);

  const filterItems = (category) => {
    if (category === "ALL") {
      setMenu(data);
      return;
    }
    const newItems = data.filter(item => item.category === category);
    setMenu(newItems);
  }

  return (
    <main>
      <section className="menu section">
        <div className="title">
          <h2>Our Menu</h2>
          <div className="underline"></div>
        </div>
        <Categories filterItems={filterItems} categories={categories} />
        <Menu items={menu} />
      </section>
    </main>
  )
}

export default App
