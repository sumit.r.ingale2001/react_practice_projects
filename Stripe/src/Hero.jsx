/* eslint-disable no-unused-vars */
import phoneImg from './assets/phone.svg';
import { useGlobalContext } from './context'

const Hero = () => {
    const {closeSubmenu} = useGlobalContext();
    return (
        <section className="hero">
            <div className="hero-center">
                <article className="hero-info">
                    <h1>Payments insfrastructure for the internet</h1>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eligendi voluptatum, nihil tempora ad non nemo!</p>
                    <button className='btn'>Start now</button>
                </article>
                <article className="hero-images">
                    <img src={phoneImg} className='phone-img' alt="phone" />
                </article>
            </div>
        </section>
    )
}

export default Hero
