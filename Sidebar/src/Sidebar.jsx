import { FaTimes } from "react-icons/fa"
import { social, links } from './data'
import logo from './logo.svg'
import { useGlobalContext } from "./context"

const Sidebar = () => {
    const { isSidebarOpen, closeSidebar } = useGlobalContext();
    return (
        <aside className={`${isSidebarOpen ? "show-sidebar sidebar" : "sidebar"}`}>
            <div className="sidebar-header">
                <img src={logo} alt="coding addict" />
                <button className="close-btn" onClick={() => closeSidebar()} >
                    <FaTimes />
                </button>
            </div>
            <ul className="links">
                {
                    links.map((link) => {
                        const { id, url, text } = link;
                        return <li key={id}>
                            <a href={url}>
                                {text}
                            </a>
                        </li>
                    })
                }
            </ul>
            <ul className="social-icons">
                {
                    social.map((link) => {
                        const { id, url } = link;
                        return <li key={id}>
                            <a href={url}>a</a>
                        </li>
                    })
                }
            </ul>
        </aside>
    )
}

export default Sidebar
