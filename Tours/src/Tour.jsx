/* eslint-disable react/prop-types */

import { useState } from "react"


const Tour = ({ id, name, image, price, info, removeTours }) => {
    const [readMore, setReadMore] = useState(false)

    const toggleRead = () => {
        setReadMore(!readMore)
    }


    return (
        <article className="single-tour" key={id} >
            <img src={image} alt={name} />
            <footer>
                <div className="tour-info">
                    <h4>{name}</h4>
                    <h4 className="tour-price">
                        ${price}
                    </h4>
                </div>
                <p>
                    {
                        readMore ? info : `${info.substring(0, 200)}...`
                    }
                    <button onClick={toggleRead} >
                        {
                            readMore ? "Show less" : "Show More"
                        }
                    </button>
                </p>
                <div className="delete-btn" onClick={() => removeTours(id)} >
                    Not interested
                </div>
            </footer>
        </article>
    )
}

export default Tour
