/* eslint-disable react/prop-types */
import { useEffect, useState } from "react"
// import rgbToHex from './utils'

const SingleColor = ({ rgb, index, weight, hex }) => {

    const [alert, setAlert] = useState(false)
    const bcg = rgb.join(",")
    const hexValue = `#${hex}`

    // const hex = rgbToHex(...rgb); using the rgbtohex function

    const handleAlert = () => {
        setAlert(true);
        navigator.clipboard.writeText(hexValue);
    }

    useEffect(() => {
        const timeout = setTimeout(() => {
            setAlert(false)
        }, 2000)
        return () => clearTimeout(timeout)
    }, [alert])

    return (
        <article
            className={`${index > 10 ? 'color-light' : "color"}`}
            style={{ backgroundColor: `rgb(${bcg})` }}
            onClick={handleAlert}
        >
            <p className="percent-value">{weight}%</p>
            <p className="color-value">{hexValue}</p>
            {
                alert && <p className="alert" >Copied to clipboard</p>
            }
        </article>
    )
}

export default SingleColor
