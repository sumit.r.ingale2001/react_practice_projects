export const data = [
    {
        img: "https://images.unsplash.com/photo-1687890425842-55d8928ecb7a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=387&q=80",
        title: "Rabbit Image",
        desc: " Lorem, ipsum dolor sit amet consectetur adipisicing elit. Recusandae, ipsam! Eum doloribus exercitationem quia debitis?"
    },
    {
        img: "https://images.unsplash.com/photo-1470071459604-3b5ec3a7fe05?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=974&q=80",
        title: "Nature",
        desc: " Lorem, ipsum dolor sit amet consectetur adipisicing elit. Recusandae, ipsam! Eum doloribus exercitationem quia debitis?"
    },
    {
        img: "https://images.unsplash.com/photo-1647891938250-954addeb9c51?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=387&q=80",
        title: "Peace",
        desc: " Lorem, ipsum dolor sit amet consectetur adipisicing elit. Recusandae, ipsam! Eum doloribus exercitationem quia debitis?"
    },
    {
        img: "https://images.unsplash.com/photo-1474623809196-26c1d33457cc?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80",
        title: "Sky diving",
        desc: " Lorem, ipsum dolor sit amet consectetur adipisicing elit. Recusandae, ipsam! Eum doloribus exercitationem quia debitis?"
    },
    {
        img: "https://images.unsplash.com/photo-1548032885-b5e38734688a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=356&q=80",
        title: "Ocean",
        desc: " Lorem, ipsum dolor sit amet consectetur adipisicing elit. Recusandae, ipsam! Eum doloribus exercitationem quia debitis?"
    },
]