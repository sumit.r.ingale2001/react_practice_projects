import { useEffect, useState } from "react"
import Home from "./components/Home"
import Navbar from "./components/Navbar"


function App() {
  const [toggleMode, setToggleMode] = useState(false)
  toggleMode ? document.title = "Dark mode website- Dark mode enabled" : "Darkmode website"
  if(toggleMode === true){
    document.title = "Darkmode websiite - Dark mode enabled"
  }else if(toggleMode === false){
    document.title = "Darkmode website"
  }else{
    document.title="Darkmode website"
  }
  return (
    <>
      <Navbar toggleMode={toggleMode} setToggleMode={setToggleMode} />
      <Home toggleMode={toggleMode} />
    </>
  )
}

export default App
