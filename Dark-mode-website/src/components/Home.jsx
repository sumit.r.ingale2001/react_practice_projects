/* eslint-disable react/prop-types */
import { data } from "../data"

const Home = ({ toggleMode }) => {
    return (
        <div id="home" className={`${toggleMode ? "bg-secondary" : "bg-light"} p-2`}>
            <div className="container">
                <div className="row">
                    <div className="col-lg-12" style={{ display: "flex", flexWrap: "wrap" }}>
                        {
                            data.map((item) => {
                                const { img, title, desc } = item;
                                return (

                                    <div className={`${toggleMode ? "bg-dark" : "bg-light"} card`} key={img} style={{ width: "18rem", margin: "10px" }}>
                                        <img src={img} className="card-img-top" alt="img" style={{ height: "10rem", objectFit: "cover", objectPosition: "center" }} />
                                        <div className={`${toggleMode ? "text-light" : "text-dark"} card-body`}>
                                            <h5 className="card-title">{title}</h5>
                                            <p className="card-text">{desc}</p>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home
