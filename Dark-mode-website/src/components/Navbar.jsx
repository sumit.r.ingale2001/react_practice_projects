/* eslint-disable react/prop-types */


const Navbar = ({ setToggleMode, toggleMode }) => {
    return (
        <div id="navbar" className={`${toggleMode ? "bg-dark" : "bg-light"} p-2`}>
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <nav className="navbar navbar-expand-lg">
                            <div className="container-fluid">
                                <a className={`${toggleMode ? "navbar-brand text-light" : "navbar-brand text-dark"}`} href="#">Modes</a>
                                <button className={`${toggleMode ? "navbar-toggler bg-light" : "navbar-toggler bg-dark"}`} type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="navbar-toggler-icon"></span>
                                </button>
                                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                        <li className="nav-item">
                                            <a className={`${toggleMode ? "nav-link text-light" : "nav-link text-dark"}`} aria-current="page" href="#">Home</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className={`${toggleMode ? "nav-link text-light" : "nav-link text-dark"}`} href="#">Services</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className={`${toggleMode ? "nav-link text-light" : "nav-link text-dark"}`} href="#">About</a>
                                        </li>
                                    </ul>

                                    <div className="form-check form-switch">
                                        <input className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked={toggleMode ? true : false} onClick={() => setToggleMode(!toggleMode)} />
                                        <label htmlFor="modes" className={`${toggleMode ? "text-light" : "text-dark"}`}>{toggleMode ? "Enable light mode" : "Enable Dark mode"}</label>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Navbar
