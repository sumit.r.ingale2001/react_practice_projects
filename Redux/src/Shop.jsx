/* eslint-disable react/prop-types */

import { useDispatch, useSelector } from "react-redux"
import { bindActionCreators } from "redux";
import { actionCreators } from './state/index'


const Shop = () => {

    const dispatch = useDispatch();
    const { depositMoney, withdrawMoney } = bindActionCreators(actionCreators, dispatch)
    const balance = useSelector(state => state.amount)

    return (
        <div>
            <h2>Deposit/withdraw money</h2>
            <button className="btn btn-primary" onClick={() => {
                withdrawMoney(100)
            }} >-</button>
            <span className="mx-2">
                Update balance {balance}
            </span>
            <button
                className="btn btn-primary"
                onClick={() => {
                    depositMoney(100)
                }}
            >+</button>
        </div>
    )
}

export default Shop
